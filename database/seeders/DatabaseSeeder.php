<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        DB::table('sales')->insert(
            [
                'id_kunjungan' => '1',
                'nama_sales' => 'Budi',
                'tgl_kunjungan' => '2022-06-25',
                'nama_customer' => 'Customer 1',
            ]
        );

        DB::table('sales')->insert(
            [
                'id_kunjungan' => '2',
                'nama_sales' => 'Budi',
                'tgl_kunjungan' => '2022-06-26',
                'nama_customer' => 'Customer 2',
            ]
        );

        DB::table('sales')->insert(
            [
                'id_kunjungan' => '3',
                'nama_sales' => 'Budi',
                'tgl_kunjungan' => '2022-06-27',
                'nama_customer' => 'Customer 3',
            ]
        );
    }
}
