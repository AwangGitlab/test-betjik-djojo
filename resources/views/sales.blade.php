<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

        <title>Test Betjik Djojo</title>

    </head>
    <style>
        th, td {
            text-align: center;
        }
    </style>
    <body>
        <table class="table table-responsive-xl">
            <thead class="thead-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Tanggal Kunjungan</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $data_sales)
                @php
                    $tgl_input = date('d-m-Y');
                    $lokasi_input = $kota." ".$prov." ".$negara." (". $long.", ".$lat.")";
                    $tanggal = date('d-m-Y', strtotime($data_sales['tgl_kunjungan']) );
                    $now = date('d-m-Y');
                @endphp
                <tr>
                    <td>{{$data_sales['id_sales']}}</td>
                    <td>{{$data_sales['nama_sales']}}</td>
                    <td>{{ $tanggal }}</td>
                    <td>{{$data_sales['nama_customer']}}</td>
                    <td>
                        @if ($tanggal == $now)
                            <button type="button" class="btn btn-primary w-100" data-toggle="modal" data-target="#exampleModal">Hadiri Meeting</button>
                        @elseif ($tanggal < $now)
                            <div class="alert alert-secondary w-100 mx-auto" role="alert">
                                Meeting Selesai
                            </div>
                        @else
                            <div class="alert alert-warning w-100 mx-auto" role="alert">
                                Akan Datang
                            </div>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Isi Data Kunjungan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/sales/post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="lokasi">Lokasi</label>
                        <input type="text" class="form-control" id="lokasi" name="lokasi" value="{{$lokasi_input}}" readonly>
                        <small id="emailHelp" class="form-text text-muted">Auto Generated Location</small>
                    </div>
                    <div class="form-group">
                        <label for="foto">Foto</label>
                        <input type="file" class="form-control-file" id="foto" name="foto" accept="image/*">
                    </div>
                    
                    <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="{{$tgl_input}}" value="{{$tgl_input}}" hidden>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>  
                </form>
            </div>
          </div>
        </div>
      </div>
</html>
