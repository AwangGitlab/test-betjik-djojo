<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Sales;
use Stevebauman\Location\Facades\Location;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // dd(Location::get('36.73.213.128'));
        $track = Location::get('36.73.213.128');
        
        $kota = $track->cityName;
        $prov = $track->regionName;
        $negara = $track->countryName;
        $lat = $track->latitude;
        $long = $track->longitude;
        
        
        $data = Sales::all();
        return view('sales', ['data'=>$data, 'kota'=>$kota, 'prov'=>$prov, 'negara'=>$negara, 'lat'=>$lat, 'long'=>$long]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return dd($request);
       $path = $request->file('foto')->store('lokasi_kunjungan');

        DB::table('kunjungan')->insert([
            'tgl_kunjungan' => $request->tanggal,
            'lokasi_kunjungan' => $request->lokasi,
            'foto_kunjungan' => $path
        ]);
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
