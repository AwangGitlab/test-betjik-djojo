<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    use HasFactory;

    protected $table = 'sales';
    protected $fillable = [
        'id_kunjungan',
        'nama_sales',
        'tgl_kunjungan',
        'nama_customer'
    ];
}
